package firstproj;;

class NumeroPrimo {

    private int num;

    public NumeroPrimo(int num){

        this.num = num;
    }

    public void setNum(int num){

        this.num = num;
    }

    public int getNum(){

        return this.num;
    }

    public boolean verificaPrimo(){
        
        int n = getNum();

        for(int i = 2; i < n; i++){
            for(int j = 2; j < n; j++){
                if(i * j == n){
                    return true;
                } 
            }
        }
        return false;
    }

}